﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Library.Models;
using Library.Services;

namespace Library.Client
{
    class Program
    {
        static IPEndPoint IPEndPoint { get; set; } = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3231);
        static async Task Main(string[] args)
        {
            using (TcpClient client = new TcpClient(IPEndPoint))
            {
                client.Connect(IPEndPoint);

                byte[] data = new byte[1024];
                NetworkStream stream = client.GetStream();

                while (stream.DataAvailable)
                {
                    int bytes = await stream.ReadAsync(data);
                }
            }
        }
    }
}
