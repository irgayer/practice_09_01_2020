﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Models
{
    public class Book
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Author { get; set; }
        public string Title { get; set; }
    }
}
