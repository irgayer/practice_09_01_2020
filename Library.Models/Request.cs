﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace Library.Models
{
    public class Request<T>
    {
        public string Give { get; set; }
        public string Add { get; set; }
        public string Update { get; set; }
        public string Remove { get; set; }
        public T Data { get; set; }
    }
}
