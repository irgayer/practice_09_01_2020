﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Library.Models
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Login { get; set; }
        public string Password { get; set; }
        public List<Book> Books { get; set; }
    }
}
