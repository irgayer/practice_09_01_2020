﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Library.Models;

namespace Library.Services
{
    public class Formatter
    {
        public string ToFomat(List<User> users)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<string> properties = typeof(User).GetProperties().Select(p => p.Name).ToList();
            List<List<string>> values = new List<List<string>>();
            
            values.Add(new List<string>());
            values.Add(new List<string>());
            values.Add(new List<string>());
            values.Add(new List<string>());
            foreach (var user in users)
            {
                values[0].Add(user.Id.ToString());
                values[1].Add(user.Login);
                values[2].Add(user.Password);
                values[3].Add(user.Books.Count.ToString());
            }

            stringBuilder.Append("[mgml]\n");
            stringBuilder.Append(MakeHeader(properties));
            stringBuilder.Append(MakeBody(values));
            stringBuilder.Append("[/mgml]");

            return stringBuilder.ToString();
        }

        public string ToFormat(List<Book> books)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<string> properties = typeof(Book).GetProperties().Select(p => p.Name).ToList();
            List<List<string>> values = new List<List<string>>();
            
            values.Add(new List<string>());
            values.Add(new List<string>());
            values.Add(new List<string>());
            foreach (var book in books)
            {
                values[0].Add(book.Id.ToString());
                values[1].Add(book.Author);
                values[2].Add(book.Title);
            }

            stringBuilder.Append("[mgml]\n");
            stringBuilder.Append(MakeHeader(properties));
            stringBuilder.Append(MakeBody(values));
            stringBuilder.Append("[/mgml]");

            return stringBuilder.ToString();
        }

        public string FromFormat(string mgml, int tableWidth)
        {
            StringBuilder stringBuilder = new StringBuilder();

            List<string> helements = new List<string>();
            List<List<string>> belements = new List<List<string>>();
            
            stringBuilder.Append(JustLine(tableWidth));



            return null;
        }
        private string JustLine(int tableWidth)
        {
            return new string('-', tableWidth);
        }
        private string JustRow(int tableWidth, params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("|");
            //string row = "|";

            foreach (string column in columns)
            {
                stringBuilder.Append(AlignCentre(column, width) + "|");
            }

            return stringBuilder.ToString();
        }
        private string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }

        private string MakeHeader(List<string> properties)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("\t[header]\n");

            foreach (var property in properties)
            {
                stringBuilder.Append($"\t\t[helement = {property} /]\n");
            }

            stringBuilder.Append("\t[/header]\n");

            return stringBuilder.ToString();
        }

        private string MakeBody(List<List<string>> bodyElements)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("\t[body]\n");

            foreach (var elements in bodyElements)
            {
                stringBuilder.Append($"\t\t[belements]\n");
                stringBuilder.Append(MakeBodyElements(elements));
                stringBuilder.Append($"\t\t[/belements]\n");
            }

            stringBuilder.Append("\t[/body]\n");

            return stringBuilder.ToString();
        }

        private string MakeBodyElements(List<string> elements)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (var element in elements)
            {
                stringBuilder.Append($"\t\t\t[belement = {element} /] \n");
            }

            return stringBuilder.ToString();
        }

    }
}
